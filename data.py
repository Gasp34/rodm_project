# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 10:09:29 2021

@author: Gaspard
"""

import pandas as pd
import numpy as np

df = pd.read_csv("data/adult.csv")

keys = df.keys()
print(keys)

a = df.to_numpy()
s = a[:,-1]

c=14 #change this
print(keys[c])
v = a[:,c]
cat,count = np.unique(v,return_counts=True)
res = np.zeros((len(cat),2))
for i in range(len(v)):
    i_cat = a[i,c]
    i_sal = a[i,-1]
    
    for j in range(len(cat)):
        if i_cat == cat[j]:
            break
    
    if i_sal == " <=50K":
        res[j,0]+=1
    else:
        res[j,1]+=1
        
ecart = np.zeros((len(cat)))
for j in range(len(cat)):
    ecart[j] = (res[j,0]-res[j,1])/res[j,0]
    
sort = np.argsort(ecart)
print("categorie : <=50K - >50K")
for j in sort:
    print(f"{cat[j]} : {int(res[j,0])}-{int(res[j,1])}, écart : {round(100*ecart[j],1)}%")