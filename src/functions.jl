# Tolerance
epsilon = 0.0001

"""
Function used to transform a column with numerical values into one or several binary columns.

Arguments:
 - data: table which contains the column that will be binarized (1 row = 1 individual, 1 column = 1 feature);
 - header: header of the column of data that will be binarized
 - intervals: array of values which delimits the binarization (ex : [2, 4, 6, 8] will lead to 3 columns respectively equal to 1 if the value of column "header" is in [2, 3], [4, 5] and [6, 7])

 Example:
  createColumns(:Age, [1, 17, 50, Inf], data, features) will create 3 binary columns in features named "Age1-16", "Age17-49", "Age50-Inf"
"""
function createColumns(header::Symbol, intervals, data::DataFrames.DataFrame, features::DataFrames.DataFrame)
    for i in 1:size(intervals, 1) - 1
        lb = intervals[i]
        ub = intervals[i+1]
        features[!, Symbol(header, lb, "-", (ub-1))] = ifelse.((data[!, header] .>= lb) .& (data[!, header] .< ub), 1, 0) 
    end
end

"""
Create the train and test csv files of a data set

Arguments:
 - dataFolder: folder which contains the data set csv file (ex: "./data")
 - dataSet: name of the data set (ex: "titanic")

Important remark: the first column of the output files must correspond to the class of each individual (and it must be 0 or 1)
"""
function createFeatures(dataFolder::String, dataSet::String)

    # Get the input file path
    rawDataPath = dataFolder * dataSet * ".csv"

    # Test its existence
    if !isfile(rawDataPath)
        println("Error in createFeatures: Input file not found: ", rawDataPath)
        return
    end

    # Put the data in a DataFrame variable
    rawData = CSV.File(rawDataPath,  header=true) |> DataFrame

    # Output files path
    trainDataPath = dataFolder * dataSet * "_train.csv"
    testDataPath = dataFolder * dataSet * "_test.csv"

    # If the train or the test file do not exist
    if !isfile(trainDataPath) || !isfile(testDataPath)

        # Shuffle the individuals rawData
        rawData = rawData[shuffle(1:size(rawData, 1)),:]

        # Percentage of data in the train set
        trainPercentage = 2/3

        if dataSet == "adult"
            trainPercentage = 1/32
        end
        
        if dataSet == "nurse_final"
            trainPercentage = 1/12
        end

        # Get the rawDatas which are in the train set 
        # (trainRawData can be used to chose select the features)
        trainLimit = trunc(Int, size(rawData, 1) * trainPercentage)
        trainRawData = rawData[1:trainLimit, :]

        println("=== Creating the features")

        # Create the table that will contain the features
        features::DataFrame = DataFrames.DataFrame()
        
        # Create the features of the titanic data set
        if dataSet == "titanic"

            # Add the column related to the class (always do it first!)
            # Remark: here the rawData already contain 0/1 values so no change needed
            features.Survived = rawData.Survived

            #### First example of the binarization of a column with numerical values
            # Add columns related to the ages
            # -> 3 columns ([0, 16], [17, 49], [50, +infinity[)
            # Ex : if the age is 20, the value of these 3 columns will be 0, 1 and 0, respectively.
            createColumns(:Age, [0, 17, 50, Inf], rawData, features)
            
            # Add columns related to the fares
            # -> 3 columns ([0, 9], [10, 19], [20, +infinity[)
            createColumns(:Fare,  [0, 10, 20, Inf], rawData, features)
            
            #### First example of the binarization of a column with categorical values
            # Add 1 column for the sex (female or not)
            # Detailed description of the command:
            # - create in DataFrame "features" a column named "Sex"
            # - for each row of index i of "rawData", if column "Sex" is equal to "female", set the value of column "Sex" in row i of features to 1; otherwise set it to 0
            features.Sex = ifelse.(rawData.Sex .== "female", 1, 0)
            
            # Add columns related to the passenger class
            # -> 3 columns (class 1, class 2 and class 3)
            
            # For each existing value in the column "Pclass"
            for a in sort(unique(rawData.Pclass))

                # Create 1 feature column named "Class1", "Class2" or "Class3"
                features[!, Symbol("Class", a)] = ifelse.(rawData.Pclass .<= a, 1, 0)
            end

            # Add a column related  to the number of relatives
            # -> 1 column (0: no relatives, 1: at least one relative)
            features.Relative = ifelse.(rawData[!, Symbol("Siblings/Spouses Aboard")] + rawData[!, Symbol("Parents/Children Aboard")] .> 0, 1, 0)

        end

        if dataSet == "adult"

            # Warning: this must remain the first column added to features
            features.income = ifelse.(rawData.income .== " <=50K", 0, 1)
            
            #age
            #createColumns(:age, [0,25,35,45,55,Inf], rawData, features)
            createColumns(:age, [0,30,50,Inf], rawData, features)
            
            #workclass
            # features.WorkclassPrivate = ifelse.(rawData.workclass .== " Private", 1, 0)
            # features.WorkclassSelfEmpInc = ifelse.(rawData.workclass .== " Self-emp-inc", 1, 0)
            # features.WorkclassGov = ifelse.((rawData.workclass .== " Federal-gov") .| (rawData.workclass .== " Local-gov") .| (rawData.workclass .== " State-gov"), 1, 0)
            # features.WorkclassSelfNotInc = ifelse.(rawData.workclass .== " Self-emp-not-inc", 1, 0)
            # features.WorkclassRest = ifelse.((rawData.workclass .== " ?") .| (rawData.workclass .== " Never-worked") .| (rawData.workclass .== " Without-pay"), 1, 0)
            features.WorkclassSelfEmpInc = ifelse.(rawData.workclass .== " Self-emp-inc", 1, 0)
            features.WorkclassGov = ifelse.((rawData.workclass .== " Federal-gov") .| (rawData.workclass .== " Local-gov") .| (rawData.workclass .== " State-gov"), 1, 0)
            features.WorkClassRest = ifelse.((features.WorkclassSelfEmpInc .== 0) .& (features.WorkclassGov .== 0), 1, 0)
        
            #education_num
            #createColumns(:education_num, [1,9,13,Inf], rawData, features)
            createColumns(:education_num, [1,9,Inf], rawData, features)
            #features.Edu = ifelse.(rawData.education_num .> 9, 1, 0)
            
            #marital_status
            features.MarriedCS = ifelse.(rawData.marital_status .== " Married-civ-spouse", 1, 0)
            #features.NotMarriedCS = ifelse.(rawData.marital_status .!= " Married-civ-spouse", 1, 0)
            
            #occupation
            features.Occ1 = ifelse.((rawData.occupation .== " Exec-managerial") .| (rawData.occupation .== " Prof-specialty"), 1, 0)
            #features.Occ2 = ifelse.((rawData.occupation .== " Protective-serv") .| (rawData.occupation .== " Tech-support") .| (rawData.occupation .== " Sales") .| (rawData.occupation .== " Craft-repair") .| (rawData.occupation .== " Transport-moving"), 1, 0)
            #features.Occ3 = ifelse.((features.Occ1 .== 0) .& (features.Occ2 .== 0), 1, 0)
            
            
            #relashion_ship
            features.WifeHusband = ifelse.((rawData.relationship .== " Wife") .| (rawData.relationship .== " Husband"), 1, 0)
            #features.NotWifeHusband = ifelse.(features.WifeHusband .==0 , 1, 0)
            
            #Race
            features.WhiteAsian = ifelse.((rawData.race .== " Asian-Pac-Islander") .| (rawData.race .== " White"), 1, 0)
            #features.NotWhiteAsian = ifelse.(features.WhiteAsian .== 0, 1, 0)
            
            #sex
            features.Male = ifelse.(rawData.sex .== " Male", 1, 0)
            #features.Female = ifelse.(rawData.sex .== " Female", 1, 0)

            #capital_gain
            #features.CG0 = ifelse.(rawData.capital_gain .== 0, 1, 0)
            #features.CGInf = ifelse.((rawData.capital_gain .!= 0) .& (rawData.capital_gain .< 5100), 1, 0)
            #features.CGSup = ifelse.((rawData.capital_gain .!= 0) .& (rawData.capital_gain .>= 5100), 1, 0)
            #features.CGInf = ifelse.(rawData.capital_gain .< 5100, 1, 0)
            features.CGSup = ifelse.(rawData.capital_gain .>= 5100, 1, 0)
            
            #hours_per_week
            #features.Inf40 = ifelse.(rawData.hours_per_week .<= 40, 1, 0)
            #features.Sup40 = ifelse.(rawData.hours_per_week .> 40, 1, 0)
            
        end
        
        if dataSet == "nurse_final"
            features.nursery = rawData.id
            
            features.parent = ifelse.((rawData.parents_great_pret .== 1) .| (rawData.parents_pretentious .== 1), 1, 0)
            
            features.nurs_crit = ifelse.((rawData.has_nurs_critical .== 1) .| (rawData.has_nurs_very_crit .== 1), 1, 0)
            features.nurs_proper = ifelse.((rawData.has_nurs_proper .== 1) .| (rawData.has_nurs_less_proper .== 1), 1, 0)
            #features.nurs_improper = rawData.has_nurs_improper
            
            features.form_comp = ifelse.((rawData.form_complete .== 1) .| (rawData.form_completed .== 1), 1, 0)
            #features.form_not_comp = ifelse.((rawData.form_foster .== 1) .| (rawData.form_incomplete .== 1), 1, 0)
            
            features.child12 = ifelse.((rawData.children_1 .== 1) .| (rawData.children_2 .== 1), 1, 0)
            #features.child3more = ifelse.((rawData.children_3 .== 1) .| (rawData.children_more .== 1), 1, 0)
            
            features.housing_conv = rawData.housing_convenient
            features.housing_crit = rawData.housing_critical
            
            features.finances = rawData.finance_convenient
            
            features.social_prob = rawData.social_problematic
            features.social_non_prob = rawData.social_nonprob
            
            features.health_recom = rawData.health_recommended
            features.health_not_recom = rawData.health_not_recom
            
        end

        # Shuffle the individuals
        features = features[shuffle(1:size(features, 1)),:]

        # Split them between train and test sets
        train = features[1:trainLimit, :]
        test = features[(trainLimit+1):end, :]
        
        CSV.write(trainDataPath, train)
        CSV.write(testDataPath, test)

        CSV.write(dataFolder * dataSet * "_train_raw.csv", rawData[1:trainLimit, :])
        CSV.write(dataFolder * dataSet * "_test_raw.csv", rawData[(trainLimit+1):end, :])

    # If the train and test file already exist
    else
        println("=== Warning: Existing features found, features creation skipped")
        println("=== Loading existing features")
        train = CSV.File(trainDataPath) |> DataFrame
        test = CSV.File(testDataPath) |> DataFrame
    end
    
    println("=== ... ", size(train, 1), " individuals in the train set")
    println("=== ... ", size(test, 1), " individuals in the test set")
    println("=== ... ", size(train, 2), " features")
    
    return train, test
end 


"""
Create the association rules related to a training set

Arguments
 - dataSet: name of the data ste
 - resultsFolder: name of the folser in which the rules will be written
 - train: DataFrame with the training set (each line is an individual, the first column is the class, the other are the features)

Output
 - table of rules (each line is a rule, the first column corresponds to the rules class)
"""
function createRules(dataSet::String, resultsFolder::String, train::DataFrames.DataFrame)

    # Create the result folder if necessary
    if !isdir(resultsFolder)
        mkdir(resultsFolder)
    end

    # Output file
    rulesPath = resultsFolder * dataSet * "_rules.csv"
    rules = DataFrame()

    if !isfile(rulesPath)

        println("=== Generating the rules")

        # Transactions
        t::DataFrame = train[:, 2:end]

        # Class of the transactions
        # Help: to get the class of transaction nb i:
        # - do not use: transactionClass[i]
        # - use: transactionClass[i, 1]
        transactionClass::DataFrame = train[:, 1:1]

        # Number of features
        d::Int64 = size(t, 2)

        # Number of transactions
        n::Int64 = size(t, 1)

        mincovy::Float64 = 0.05
        iterlim::Int64 = 5
        RgenX::Float64 = 0.1 / n
        RgenB::Float64 = 0.1 / (n * d)
        nb_rule = 0
        
        ##################
        # Find the rules for each class
        ##################
        for y = 0:2

		println("-- Classe $y")

	     # TODO
		 
            ## Hint 1 
            # Each time you solve the model for a class y, the value of variable b represents a rule.
            # To convert the value of b into a DataFrame, you can use the following instruction: 
            # rule = DataFrame(hcat(append!([y], trunc.(Int, JuMP.value.(b)))...), :auto)
            #
            #       Here are some explanations (optional):
            #       - trunc.(Int, JuMP.value.(b)): get the value of b in the current solution and truncate it.
            #       - append!([y], x): add y at the beginning of vector x
            #       - hcat(x...): convert vector x into a matrix in which the only line corresponds to x
            #       - DataFrame(x, :auto): convert a matrix into a data frame
            #
            # The rule can then be appended to rules:
            # append!(rules, rule)
            
		# cf. algorithm of the lecture (slide 35)
		rule = DataFrame()
		iter = 1
		cmax = n
        s_sol = 0
		
		# Create the model
		m = Model(CPLEX.Optimizer)
		set_optimizer_attribute(m, "CPX_PARAM_SCRIND", 0)				
		@variable(m, b[1:d], Bin)
		@variable(m, 0 <= x[1:n] <= 1)
		@objective(m, Max, sum(x[i] for i in 1:n if transactionClass[i,1] == y) -  RgenX * sum(x[i] for i in 1:n) - RgenB * sum(b[j] for j in 1:d))
		@constraint(m, [i in 1:n, j in 1:d], x[i] <= 1 + (t[i,j]-1)*b[j])
		@constraint(m, [i in 1:n], x[i] >= 1 + sum((t[i,j]-1)*b[j] for j in 1:d))
		@constraint(m,CMAX,sum(x[i] for i in 1:n) <= cmax)
				
		while cmax >= n * mincovy
			println("cmax $cmax, iter $iter, nb_rule $nb_rule")
			
			if iter == 1
				optimize!(m)
                s_sol = trunc(Int,sum(JuMP.value.(x)[i] for i in 1:n if transactionClass[i,1] == y))
				iter +=1
			end
			println("s_sol $s_sol")
            
			b_sol = trunc.(Int, JuMP.value.(b))
			rule = DataFrame(hcat(append!([y], b_sol)...), :auto)
			append!(rules, rule)
			@constraint(m, sum(b[j] for j in 1:d if b_sol[j] == 0) + sum((1-b[j]) for j in 1:d if b_sol[j] == 1) >= 1)
			nb_rule += 1
            
			if iter < iterlim
				optimize!(m)
                sTemp = trunc(Int,sum(JuMP.value.(x)[i] for i in 1:n if transactionClass[i,1] == y))
                
                println("sTemp $sTemp")
                
				if sTemp < s_sol
					cmax = min(cmax-1, sum(JuMP.value.(x)[:]))
                    println("sum $(sum(JuMP.value.(x)[:]))")
                    
                    set_normalized_rhs(CMAX, cmax)
					iter = 1
				else
					iter += 1
				end
			else
				cmax -= 1
                set_normalized_rhs(CMAX, cmax)
				iter = 1
			end
		end
        end

        CSV.write(rulesPath, rules)

    else
        println("=== Warning: Existing rules found, rules creation skipped")
        println("=== Loading the existing rules")
        rules = CSV.File(rulesPath) |> DataFrame
    end
    
    println("=== ... ", size(rules, 1), " rules obtained") 

    return rules
end

"""
Sort the rules

Arguments
  - dataSet: name of the dataset folder
  - resultsFolder: name of the folder in which the results are written
  - train: train data set (1 row = 1 individual)
  - rules: rules which must be sorted (1 row = 1 rule)
  - tilim: maximal running time of CPLEX in seconds
"""
function sortRules(dataSet::String, resultsFolder::String, train::DataFrames.DataFrame, rules::DataFrames.DataFrame, tilim::Int64)

    orderedRulesPath = resultsFolder * dataSet * "_ordered_rules.csv"

    if !isfile(orderedRulesPath)

        println("=== Sorting the rules")
        
        # Transactions
        t = train[:, 2:end]

        # Class of the transactions
        transactionClass = train[:, 1:1]

        # Number of features
        d = size(t, 2)

        # Number of transactions
        n = size(t, 1)

        # Add the two null rules in first position
        nullrules = similar(rules, 0)
        push!(nullrules, append!([0], zeros(d)))
        push!(nullrules, append!([1], zeros(d)))
        rules = vcat(nullrules, rules)

        # Remove duplicated rules
        rules = unique(rules)

        # Number of rules
        L = size(rules)[1]

        Rrank = 1/L

        ################
        # Compute the v_il and p_il constants
        # p_il = :
        #  0 if rule l does not apply to transaction i
        #  1 if rule l applies to transaction i and   correctly classifies it
        # -1 if rule l applies to transaction i and incorrectly classifies it
        ################
        p = zeros(n, L)

        # For each transaction and each rule
        for i in 1:n
            for l in 1:L

                # If rule l applies to transaction i
                # i.e., if the vector t_i - r_l does not contain any negative value
                if !any(x->(x<-epsilon), [sum(t[i, k]-rules[l, k+1]) for k in 1:d])

                    # If rule l correctly classifies transaction i
                    if transactionClass[i, 1] == rules[l, 1]
                        p[i, l] = 1
                    else
                        p[i, l] = -1 
                    end
                end
            end
        end

        v = abs.(p)
        
        ################
        # Create and solve the model
        ###############
        #CPX_PARAM_SCRIND=0
        m = Model(CPLEX.Optimizer)
        set_optimizer_attribute(m, "CPX_PARAM_TILIM", tilim)
        
        # u_il: rule l is the highest which applies to transaction i
        @variable(m, u[1:n, 1:L], Bin)

        # r_l: rank of rule l
        @variable(m, 1 <= r[1:L] <= L, Int)

        # rstar: rank of the highest null rule
        @variable(m, 1 <= rstar <= L)
        @variable(m, 1 <= rB <= L)

        # g_i: rank of the highest rule which applies to transaction i
        @variable(m, 1 <= g[1:n] <= L, Int)

        # s_lk: rule l is assigned to rank k
        @variable(m, s[1:L,1:L], Bin)

        # Rank of null rules
        rA = r[1]
        rB = r[2]

        # rstar == rB?
        @variable(m, alpha, Bin)

        # rstar == rA?
        @variable(m, 0 <= beta <= 1)

        # Maximize the classification accuracy
        @objective(m, Max, sum(p[i, l] * u[i, l] for i in 1:n for l in 1:L)
                   + Rrank * rstar)

        # Only one rule is the highest which applies to transaction i
        @constraint(m, [i in 1:n], sum(u[i, l] for l in 1:L) == 1)

        # g constraints
        @constraint(m, [i in 1:n, l in 1:L], g[i] >= v[i, l] * r[l])
        @constraint(m, [i in 1:n, l in 1:L], g[i] <= v[i, l] * r[l] + L * (1 - u[i, l]))

        # Relaxation improvement
        @constraint(m, [i in 1:n, l in 1:L], u[i, l] >= 1 - g[i] + v[i, l] * r[l])
        @constraint(m, [i in 1:n, l in 1:L], u[i, l] <= v[i, l]) 

        # r constraints
        @constraint(m, [k in 1:L], sum(s[l, k] for l in 1:L) == 1)
        @constraint(m, [l in 1:L], sum(s[l, k] for k in 1:L) == 1)
        @constraint(m, [l in 1:L], r[l] == sum(k * s[l, k] for k in 1:L))

        # rstar constraints
        @constraint(m, rstar >= rA)
        @constraint(m, rstar >= rB)
        @constraint(m, rstar - rA <= (L-1) * alpha)
        @constraint(m, rA - rstar <= (L-1) * alpha)
        @constraint(m, rstar - rB <= (L-1) * beta)
        @constraint(m, rB - rstar <= (L-1) * beta)
        @constraint(m, alpha + beta == 1)

        # u_il == 0 if rstar > rl (also improve relaxation)
        @constraint(m, [i in 1:n, l in 1:L], u[i, l] <= 1 - (rstar - r[l])/ (L - 1))

        status = optimize!(m)

        ###############
        # Write the rstar highest ranked rules and their corresponding class
        ###############

        # Number of rules kept in the classifier
        # (all the rules ranked lower than rstar are removed)
        relevantNbOfRules=L-trunc(Int, JuMP.value(rstar))+1

        # Sort the rules and their class by decreasing rank
        rulesOrder = JuMP.value.(r)
        orderedRules = rules[sortperm(L.-rulesOrder), :]

        orderedRules = orderedRules[1:relevantNbOfRules, :]

        CSV.write(orderedRulesPath, orderedRules)

    else
        println("=== Warning: Sorted rules found, sorting of the rules skipped")
        println("=== Loading the sorting rules")
        orderedRules = CSV.File(orderedRulesPath) |> DataFrame
    end 

    return orderedRules

end

"""
Compute for a given data set the precision and the recall of 
- each class
- the whole data set (with and without weight for each class)

Arguments
  - orderedRules: list of rules of the classifier (1st row = 1st rule to test)
  - dataset: the data set (1 row = 1 individual)
"""
function showStatistics(orderedRules::DataFrames.DataFrame, dataSet::DataFrames.DataFrame)

    
    # Number of transactions
    n = size(dataSet, 1)

    # Statistics with respect to class 0:
    # - true positive;
    # - true negative;
    # - false positive;
    # - false negative
    tp::Int = 0
    fp::Int = 0
    fn::Int = 0
    tn::Int = 0

    # Number of individuals in each class
    classSize = Array{Int, 1}([0, 0])
    
    # For all transaction i in the data set
    for i in 1:n

        # Get the first rule satisfied by transaction i
        ruleId = findfirst(all, collect(eachrow(Array{Float64, 2}(orderedRules[:, 2:end])  .<= Array{Float64, 2}(DataFrame(dataSet[i, 2:end])))))

        # If transaction i is classified correctly (i.e., if it is a true)
        if orderedRules[ruleId, 1] == dataSet[i, 1]

            # If transaction i is of class 0
            if dataSet[i, 1] == 0
                tp += 1
                classSize[1] += 1
            else
                tn += 1
                classSize[2] += 1
            end 

            # If it is a negative
        else

            # If transaction i is of class 0
            if dataSet[i, 1] == 0
                fn += 1
                classSize[1] += 1
            else
                fp += 1
                classSize[2] += 1
            end 
        end
    end

    precision = Array{Float64, 1}([tp / (tp+fp), tn / (tn+fn)])
    recall = Array{Float64, 1}([tp / (tp + fn), tn / (tn + fp)])

        @show tp, fp, tn, fn
        @show precision, recall, classSize
    println("Class\tPrec.\tRecall\tSize")
    println("0\t", round(precision[1], digits=2), "\t", round(recall[1], digits=2), "\t", classSize[1])
    println("1\t", round(precision[2], digits=2), "\t", round(recall[2], digits=2), "\t", classSize[2], "\n")
    println("avg\t", round((precision[1] + precision[2])/2, digits=2), "\t", round((recall[1] + recall[2])/2, digits=2))
    println("w. avg\t", round(precision[1] * classSize[1] / size(dataSet, 1) + precision[2] * classSize[2] / size(dataSet, 1), digits = 2), "\t", round(recall[1] * classSize[1] / size(dataSet, 1) + recall[2] * classSize[2] / size(dataSet, 1), digits = 2), "\n")
    
end 

function showAcc(orderedRules::DataFrames.DataFrame, dataSet::DataFrames.DataFrame)

    
    # Number of transactions
    n = size(dataSet, 1)

    # Statistics with respect to class 0:
    # - true positive;
    # - true negative;
    # - false positive;
    # - false negative
    c1p::Int = 0
    c1f::Int = 0
    c2p::Int = 0
    c2f::Int = 0
    c3p::Int = 0
    c3f::Int = 0
    
    # Number of individuals in each class
    classSize = Array{Int, 1}([0, 0])
    
    # For all transaction i in the data set
    for i in 1:n

        # Get the first rule satisfied by transaction i
        ruleId = findfirst(all, collect(eachrow(Array{Float64, 2}(orderedRules[:, 2:end])  .<= Array{Float64, 2}(DataFrame(dataSet[i, 2:end])))))

        # If transaction i is classified correctly (i.e., if it is a true)
        if orderedRules[ruleId, 1] == dataSet[i, 1]

            # If transaction i is of class 0
            if dataSet[i, 1] == 0
                c1p += 1
            end
            if dataSet[i, 1] == 1
                c2p += 1
            end
            if dataSet[i, 1] == 2
                c3p += 1
            end
            # If it is a negative
        else
            if dataSet[i, 1] == 0
                c1f += 1
            end
            if dataSet[i, 1] == 1
                c2f += 1
            end
            if dataSet[i, 1] == 2
                c3f += 1
            end
        end
    end

    acc1 = c1p/(c1p+c1f)
    acc2 = c2p/(c2p+c2f)
    acc3 = c3p/(c3p+c3f)
    
    w1 = (c1p+c1f)/n
    w2 = (c2p+c2f)/n
    w3 = (c3p+c3f)/n

    println("Class\tAcc")
    println("0\t\t", round(acc1, digits=3))
    println("1\t\t", round(acc2, digits=3))
    println("2\t\t", round(acc3, digits=3))
    println("avg acc\t\t", round((acc1+acc2+acc3)/3, digits = 3))
    println("w. avg acc\t", round(acc1*w1+acc2*w2+acc3*w3, digits = 3))
    
end 

